
#src  shpe = m,30
#tgt

from model import *
src,src_tgt = Embedding("../dataset/dictionary.txt","../dataset/sentence.txt",0)
res,res_tgt = Embedding("../dataset/dictionary_2.txt","../dataset/result.txt",2)
n_tokens = (res_tgt != 1 ).sum()
# print(tgt_res.shape)
# print(n_tokens)
class TranslationLoss(nn.Module):

    def __init__(self):
        super(TranslationLoss, self).__init__()
        # 使用KLDivLoss，不需要知道里面的具体细节。
        self.criterion = nn.KLDivLoss(reduction="sum")
        self.padding_idx = 1

    def forward(self, x, target):
        """
        损失函数的前向传递
        :param x: 将Decoder的输出再经过predictor线性层之后的输出。
                  也就是Linear后、Softmax前的状态
        :param target: tgt_y。也就是label，例如[[1, 34, 15, ...], ...]
        :return: loss
        """

        """
        由于KLDivLoss的input需要对softmax做log，所以使用log_softmax。
        等价于：log(softmax(x))
        """
        x = torch.nn.functional.log_softmax(x, dim=-1)

        """
        构造Label的分布，也就是将[[1, 34, 15, ...]] 转化为:
        [[[0, 1, 0, ..., 0],
          [0, ..., 1, ..,0],
          ...]],
        ...]
        """
        # 首先按照x的Shape构造出一个全是0的Tensor
        true_dist = torch.zeros(x.size())
        # 将对应index的部分填充为1
        true_dist.scatter_(1, target.data.unsqueeze(1), 1)
        # 找出<pad>部分，对于<pad>标签，全部填充为0，没有1，避免其参与损失计算。
        mask = torch.nonzero(target.data == self.padding_idx)
        if mask.dim() > 0:
            true_dist.index_fill_(0, mask.squeeze(), 0.0)

        # 计算损失
        return self.criterion(x, true_dist.clone().detach())




model = Model()
criteria = TranslationLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=3e-4)

total_loss = 0
# out = model(src, tgt)
# out = model.predictor(out)
# print(out.shape)
# loss = criteria(out.contiguous().view(-1, out.size(-1)), tgt_res.contiguous().view(-1)) / n_tokens
# loss.backward()
# optimizer.step()
# total_loss += loss
# print("loss=",total_loss)
for step in range(20):
    # 清空梯度
    optimizer.zero_grad()
    # 进行transformer的计算
    out = model(src, res)
    # 将结果送给最后的线性层进行预测
    out = model.predictor(out)
    loss = criteria(out.contiguous().view(-1, out.size(-1)), res_tgt.contiguous().view(-1)) / n_tokens
    # 计算梯度
    loss.backward()
    # 更新参数
    optimizer.step()
    total_loss += loss
    print("Step {}, total_loss: {}".format(step, total_loss))
    total_loss = 0



torch.save(model.state_dict(),"model.pt")
