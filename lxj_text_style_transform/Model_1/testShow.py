from model import *
modelTwo = Model()
modelTwo.load_state_dict(torch.load("model.pt"))
with open("../dataset/dictionary.txt",encoding="utf-8") as Fin_0:
        train = Fin_0.read()
        dic = train.split('\n')
src,src_tgt = Embedding("../dataset/dictionary.txt","../dataset/sentence.txt",0)
res,res_tgt = Embedding("../dataset/dictionary_2.txt","../dataset/result.txt",0)
out = modelTwo(src, res)

# 将结果送给最后的线性层进行预测
out = modelTwo.predictor(out)
out = torch.nn.functional.softmax(out,dim=2)
print(out)
print("successful")
# for i in out:
#     for j in i:
#         tmp =0.0
#         site = 0
#         count =0
#         for k in j:
#             if k>tmp:
#                 tmp=k
#                 site = count
#             count= count+1
#         if dic[site]=='<END>':
#             print('\n')
#         elif dic[site]=='<PASS>':
#             a=a+1
#         elif dic[site]!='<START>':
#             print(dic[site])
for i in out:
    for j in i:
        tmp=0.0
        site=0
        count=0
        for k in j:
            if k>tmp:
                if count!=1:
                    tmp = k
                    site=count
            count= count+1
        if dic[site]=='<END>':
            print()
            break
        elif dic[site]=='<PASS>':
            a=1
        elif dic[site]!='<START>':
            print(dic[site])