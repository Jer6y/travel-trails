import copy
import numpy as np
import jieba
import numpy as np
from collections import Counter
import torch.nn as nn
import torch
#30是最大的句子长度
Max_Lenth=30
Embedding_DIM = 128 #需要Embedding之后的向量维度
Embedding_Max_Word_Size_src = 4237 #在Embedding所规定的最多词限制 必须要小于词库的数量 词库数量由dicCreator给出
Embedding_Max_Word_Size_des = 4107
#读取给定文件地址的内容
#返回列表
#列表中的每一行是一行
def load_dic(file):
    print("*********loading data********************")
    with open(file,encoding="utf-8") as Fin_0:
        text = Fin_0.read()
        List = text.split('\n')
        return List
#Embedding函数会返回对数据集进行Word2Vec编码后的向量  类型为torch.Tensor
#读取文件dictionary.txt然后创建字典 对sentence.txt和result.txt的句子进行编码
#返回的shape为(batch_size,30,Embedding_DIM) 就是有batch_size个句子 每个句子定长为30
#句子起始为<START> 结束为<END> 不足30则拼接<PASS>
def Embedding(file_dic,file_txt,mode):
    print("*********Now Embedding....***************")
    List = load_dic(file_dic)
    #print(List)
    voc = {b:a for a,b in enumerate(List)} # List转换为另一个字典
    #print(voc)
    with open(file_txt,encoding="utf-8") as Fin_0:
        train = Fin_0.read()
        trainList = train.split('\n')
    encodedTrainList = list()
    #count=0
    for txt in trainList:
        tmp = jieba.cut(txt)
        tmp = " ".join(tmp)
        tmp = tmp.split()
        TList = list()
        TList.append(voc['<START>'])
        for i in tmp:
            TList.append(voc[i])
        TList.append(voc['<END>'])
        times = Max_Lenth-len(TList)
        for i in range(times):
            TList.append(voc['<PASS>'])
      #  if len(TList)>count:
      #      count=len(TList)
        encodedTrainList.append(TList)
    tgt =copy.deepcopy(encodedTrainList)
    if mode==0:
        for i in tgt:
            i.pop()
    if mode==1:
        for i in tgt:
            i.pop(0)
    if mode==2:
        for i in encodedTrainList:
            i.pop()
        for i in tgt:
            i.pop(0)
    wordVectors_1 = torch.LongTensor(encodedTrainList)
    tgt = torch.LongTensor(tgt)
    print("*********Embedding Successfully!*********")
    # print(wordVectors_1.shape)
    # print(wordVectors_1)
    # print(wordVectors_2.shape)
    # print(wordVectors_2)
    # print(tgt.shape)
    # print(tgt)
    # print(res_tgt.shape)
    # print(res_tgt)
    return wordVectors_1,tgt
# src,src_tgt = Embedding("../dataset/dictionary.txt","../dataset/sentence.txt",0)
# print(src[0])
# print(src_tgt[0])
# res,res_tgt = Embedding("../dataset/dictionary_2.txt","../dataset/result.txt",1)
# print(res[0])
# print(res_tgt[0])