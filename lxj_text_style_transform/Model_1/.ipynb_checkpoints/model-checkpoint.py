from Embedding import *
import math
import random
import torch
import torch.nn as nn

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        # 初始化Shape为(max_len, d_model)的PE (positional encoding)
        pe = torch.zeros(max_len, d_model)
        # 初始化一个tensor [[0, 1, 2, 3, ...]]
        position = torch.arange(0, max_len).unsqueeze(1)
        # 这里就是sin和cos括号中的内容，通过e和ln进行了变换
        div_term = torch.exp(
            torch.arange(0, d_model, 2) * -(math.log(10000.0) / d_model)
        )
        # 计算PE(pos, 2i)
        pe[:, 0::2] = torch.sin(position * div_term)
        # 计算PE(pos, 2i+1)
        pe[:, 1::2] = torch.cos(position * div_term)
        # 为了方便计算，在最外面在unsqueeze出一个batch
        pe = pe.unsqueeze(0)
        # 如果一个参数不参与梯度下降，但又希望保存model的时候将其保存下来
        # 这个时候就可以用register_buffer
        self.register_buffer("pe", pe)

    def forward(self, x):
        """
        x 为embedding后的inputs，例如(3,9, 128)，batch size为3,9个单词，单词维度为128
        """
        # 将x和positional encoding相加。
        x = x + self.pe[:, : x.size(1)].requires_grad_(False)
        return self.dropout(x)

class Model(nn.Module):
    def __init__(self, d_model=128):
        super(Model, self).__init__()
        # 定义词向量
        self.embedding = nn.Embedding(num_embeddings=Embedding_Max_Word_Size, embedding_dim=Embedding_DIM)
        # 定义Transformer
        self.transformer = nn.Transformer(d_model=Embedding_DIM,nhead= 8, num_encoder_layers=6, num_decoder_layers=6, dim_feedforward=2048, batch_first=True)

        # 定义位置编码器
        self.positional_encoding = PositionalEncoding(Embedding_DIM, dropout=0)

        # 定义最后的线性层。
        # 后面的CrossEntropyLoss进行softmax
        self.predictor = nn.Linear(Embedding_DIM, Embedding_Max_Word_Size,bias=False)
    def forward(self, src, tgt):
        # 生成mask
        tgt_mask = nn.Transformer.generate_square_subsequent_mask(tgt.size()[-1])
        src_key_padding_mask = Model.get_key_padding_mask(src)
        tgt_key_padding_mask = Model.get_key_padding_mask(tgt)

        # 对src和tgt进行编码
        src = self.embedding(src)
        tgt = self.embedding(tgt)
        # 给src和tgt的token增加位置信息
        src = self.positional_encoding(src)
        tgt = self.positional_encoding(tgt)

        # 将准备好的数据送给transformer
        out = self.transformer(src, tgt,
                               tgt_mask=tgt_mask,
                               src_key_padding_mask=src_key_padding_mask,
                               tgt_key_padding_mask=tgt_key_padding_mask)

        '''
        这里直接返回transformer的结果。因为训练和推理时的行为不一样，
        所以在该模型外再进行线性层的预测。
        '''
        return out

    @staticmethod
    def get_key_padding_mask(tokens):
        '''
        用于key_padding_mask
        '''
        key_padding_mask = torch.zeros(tokens.size())
        key_padding_mask[tokens == 2] = -torch.inf
        return key_padding_mask