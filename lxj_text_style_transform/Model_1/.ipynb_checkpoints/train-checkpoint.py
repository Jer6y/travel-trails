
#src  shpe = m,30
#tgt

from model import *
src,res,tgt,tgt_res = Embedding()
n_tokens = (tgt_res != 5112 ).sum()
# print(tgt_res.shape)
# print(n_tokens)
model = Model()
criteria = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=3e-4)

total_loss = 0
# out = model(src, tgt)
# out = model.predictor(out)
# print(out.shape)
# loss = criteria(out.contiguous().view(-1, out.size(-1)), tgt_res.contiguous().view(-1)) / n_tokens
# loss.backward()
# optimizer.step()
# total_loss += loss
# print("loss=",total_loss)
for step in range(20):
    # 清空梯度
    optimizer.zero_grad()
    # 进行transformer的计算
    out = model(src, tgt)
    # 将结果送给最后的线性层进行预测
    out = model.predictor(out)
    loss = criteria(out.contiguous().view(-1, out.size(-1)), tgt_res.contiguous().view(-1)) / n_tokens
    # 计算梯度
    loss.backward()
    # 更新参数
    optimizer.step()
    total_loss += loss
    # 每40次打印一下loss
    print("Step {}, total_loss: {}".format(step, total_loss))
    total_loss = 0



torch.save(model.state_dict(),"model.pt")