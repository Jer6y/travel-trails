from model import *
modelTwo = Model()
modelTwo.load_state_dict(torch.load("model.pt"))

def Embedding():
    print("*********Now Embedding....***************")
    List = load_dic("../dataset/dictionary.txt")
    #print(List)
    voc = {b:a for a,b in enumerate(List)} # List转换为另一个字典
    #print(voc)
    with open("../dataset/sentence.txt",encoding="utf-8") as Fin_0:
        train = Fin_0.read()
        trainList = train.split('\n')
    with open("../dataset/result.txt",encoding="utf-8") as Fin_1:
        result = Fin_1.read()
        resultList = result.split('\n')
    encodedTrainList = list()
    encodedResultList = list()
    #count=0
    for txt in trainList:
        tmp = jieba.cut(txt)
        tmp = " ".join(tmp)
        tmp = tmp.split()
        TList = list()
        TList.append(voc['<START>'])
        for i in tmp:
            TList.append(voc[i])
        TList.append(voc['<END>'])
        times = Max_Lenth-len(TList)
        for i in range(times):
            TList.append(voc['<PASS>'])
      #  if len(TList)>count:
      #      count=len(TList)
        encodedTrainList.append(TList)
    for txt in resultList:
        tmp = jieba.cut(txt)
        tmp = " ".join(tmp)
        tmp = tmp.split()
        TList = list()
        TList.append(voc['<START>'])
        for i in tmp:
            TList.append(voc[i])
        TList.append(voc['<END>'])
        times = Max_Lenth-len(TList)
        for i in range(times):
            TList.append(voc['<PASS>'])
       # if len(TList)>count:
       #     count=len(TList)
        encodedResultList.append(TList)
    #print(count)
    #print(encodedTrainList)
    #print(encodedResultList)
    tgt =copy.deepcopy(encodedTrainList)
    for i in tgt:
        i.pop()
    res_tgt=copy.deepcopy(encodedResultList)
    for i in res_tgt:
        i.pop(0)
    wordVectors_1 = torch.LongTensor(encodedTrainList)
    wordVectors_2 = torch.LongTensor(encodedResultList)
    tgt = torch.LongTensor(tgt)
    res_tgt = torch.LongTensor(res_tgt)
    print("*********Embedding Successfully!*********")
    # print(wordVectors_1.shape)
    # print(wordVectors_1)
    # print(wordVectors_2.shape)
    # print(wordVectors_2)
    # print(tgt.shape)
    # print(tgt)
    # print(res_tgt.shape)
    # print(res_tgt)
    return wordVectors_1,wordVectors_2,tgt,res_tgt


with open("../dataset/dictionary.txt",encoding="utf-8") as Fin_0:
        train = Fin_0.read()
        dic = train.split('\n')
src,b,tgt,a = Embedding()
out = modelTwo(src, tgt)
# 将结果送给最后的线性层进行预测
out = modelTwo.predictor(out)
out = torch.nn.functional.softmax(out,dim=2)
print("successful")
# for i in out:
#     for j in i:
#         tmp =0.0
#         site = 0
#         count =0
#         for k in j:
#             if k>tmp:
#                 tmp=k
#                 site = count
#             count= count+1
#         if dic[site]=='<END>':
#             print('\n')
#         elif dic[site]=='<PASS>':
#             a=a+1
#         elif dic[site]!='<START>':
#             print(dic[site])
for i in out:
    for j in i:
        tmp=0.0
        site=0
        count=0
        for k in j:
            if k>tmp:
                tmp = k
                site=count
            count= count+1
        print(site)