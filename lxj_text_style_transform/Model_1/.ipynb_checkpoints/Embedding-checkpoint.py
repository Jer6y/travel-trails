import copy
import numpy as np
import jieba
import numpy as np
from collections import Counter
import torch.nn as nn
import torch
#30是最大的句子长度
Max_Lenth=30
Embedding_DIM = 128 #需要Embedding之后的向量维度
Embedding_Max_Word_Size = 5115 #在Embedding所规定的最多词限制 必须要小于词库的数量 词库数量由dicCreator给出
#读取给定文件地址的内容
#返回列表
#列表中的每一行是一行
def load_dic(file):
    print("*********loading data********************")
    with open(file,encoding="utf-8") as Fin_0:
        text = Fin_0.read()
        List = text.split('\n')
        return List
#Embedding函数会返回对数据集进行Word2Vec编码后的向量  类型为torch.Tensor
#读取文件dictionary.txt然后创建字典 对sentence.txt和result.txt的句子进行编码
#返回的shape为(batch_size,30,Embedding_DIM) 就是有batch_size个句子 每个句子定长为30
#句子起始为<START> 结束为<END> 不足30则拼接<PASS>
def Embedding():
    print("*********Now Embedding....***************")
    List = load_dic("../dataset/dictionary.txt")
    #print(List)
    voc = {b:a for a,b in enumerate(List)} # List转换为另一个字典
    #print(voc)
    with open("../dataset/sentence.txt",encoding="utf-8") as Fin_0:
        train = Fin_0.read()
        trainList = train.split('\n')
    with open("../dataset/result.txt",encoding="utf-8") as Fin_1:
        result = Fin_1.read()
        resultList = result.split('\n')
    encodedTrainList = list()
    encodedResultList = list()
    #count=0
    for txt in trainList:
        tmp = jieba.cut(txt)
        tmp = " ".join(tmp)
        tmp = tmp.split()
        TList = list()
        TList.append(voc['<START>'])
        for i in tmp:
            TList.append(voc[i])
        TList.append(voc['<END>'])
        times = Max_Lenth-len(TList)
        for i in range(times):
            TList.append(voc['<PASS>'])
      #  if len(TList)>count:
      #      count=len(TList)
        encodedTrainList.append(TList)
    for txt in resultList:
        tmp = jieba.cut(txt)
        tmp = " ".join(tmp)
        tmp = tmp.split()
        TList = list()
        TList.append(voc['<START>'])
        for i in tmp:
            TList.append(voc[i])
        TList.append(voc['<END>'])
        times = Max_Lenth-len(TList)
        for i in range(times):
            TList.append(voc['<PASS>'])
       # if len(TList)>count:
       #     count=len(TList)
        encodedResultList.append(TList)
    #print(count)
    #print(encodedTrainList)
    #print(encodedResultList)
    tgt =copy.deepcopy(encodedTrainList)
    for i in tgt:
        i.pop()
    res_tgt=copy.deepcopy(encodedResultList)
    for i in res_tgt:
        i.pop(0)
    wordVectors_1 = torch.LongTensor(encodedTrainList)
    wordVectors_2 = torch.LongTensor(encodedResultList)
    tgt = torch.LongTensor(tgt)
    res_tgt = torch.LongTensor(res_tgt)
    print("*********Embedding Successfully!*********")
    # print(wordVectors_1.shape)
    # print(wordVectors_1)
    # print(wordVectors_2.shape)
    # print(wordVectors_2)
    # print(tgt.shape)
    # print(tgt)
    # print(res_tgt.shape)
    # print(res_tgt)
    return wordVectors_1,wordVectors_2,tgt,res_tgt