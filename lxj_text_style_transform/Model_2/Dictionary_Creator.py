from Lib_Parameters import *
import jieba

def text_tokenizer(txt):
    """
    定义分词器
    :param line: 句子
    :return: 分词结果，
    """
    tmp = jieba.cut(txt)
    tmp = " ".join(tmp)
    tmp = tmp.split()
    return tmp



def yield_informal_tokens():
    file = open(informal_filepath, encoding='utf-8')
    for line in tqdm(file, desc="口语词典", total=row_count):
        yield text_tokenizer(line)
    file.close()

def yield_formal_tokens():
    file = open(formal_filepath, encoding='utf-8')
    for line in tqdm(file, desc="书面词典", total=row_count):
        yield text_tokenizer(line)
    file.close()


informal_vocab_file = work_dir / "informal_vocab.pt"
formal_vocab_file = work_dir / "formal_vocab.pt"

if use_cache and os.path.exists(informal_vocab_file):
    informal_vocab = torch.load(informal_vocab_file, map_location="cpu")
else:
    informal_vocab = build_vocab_from_iterator(
        yield_informal_tokens(),
        min_freq=1,
        specials=["<s>", "</s>", "<pad>", "<unk>"],
    )
    informal_vocab.set_default_index(informal_vocab["<unk>"])
    torch.save(informal_vocab, informal_vocab_file)

if use_cache and os.path.exists(formal_vocab_file):
    formal_vocab = torch.load(formal_vocab_file, map_location="cpu")
else:
    formal_vocab = build_vocab_from_iterator(
        yield_formal_tokens(),
        min_freq=1,
        specials=["<s>", "</s>", "<pad>", "<unk>"],
    )
    formal_vocab.set_default_index(formal_vocab["<unk>"])
    torch.save(formal_vocab, formal_vocab_file)
