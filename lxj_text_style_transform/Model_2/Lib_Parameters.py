import os
import math

import torch
import torch.nn as nn
from tokenizers import Tokenizer
from torchtext.vocab import build_vocab_from_iterator
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch.nn.functional import pad, log_softmax
from pathlib import Path
from tqdm import tqdm


# 工作目录，缓存文件盒模型会放在该目录下
work_dir = Path("./dataset")

# 训练好的模型会放在该目录下
model_dir = Path("./Model_Trained")

# 上次运行到的地方，如果是第一次运行，为None，如果中途暂停了，下次运行时，指定目前最新的模型即可。
model_checkpoint = None # 'model_10000.pt'

# 如果工作目录不存在，则创建一个
if not os.path.exists(work_dir):
    os.makedirs(work_dir)

# 如果工作目录不存在，则创建一个
if not os.path.exists(model_dir):
    os.makedirs(model_dir)

# 口语句子的文件路径
informal_filepath = '../dataset/sentence.txt'
# 正式句子的文件路径
formal_filepath = '../dataset/result.txt'


# 获取文件行数
def get_row_count(filepath):
    count = 0
    for _ in open(filepath, encoding='utf-8'):
        count += 1
    return count


# informal句子数量
informal_count = get_row_count(informal_filepath)

# formal句子数量
formal_count = get_row_count(formal_filepath)

assert informal_count == formal_count, "口语和正式语文件行数不一致！"

# 句子数量，主要用于后面显示进度。
row_count = informal_count

# 定义句子最大长度，如果句子不够这个长度，则填充，若超出该长度，则裁剪
max_length = 40
print("句子数量为：", informal_count)
print("句子最大长度为：", max_length)

# 定义informal和formal词典，都为Vocab类对象，后面会对其初始化
informal_vocab = None
formal_vocab = None

# 定义batch_size，由于是训练文本，占用内存较小，可以适当大一些
batch_size = 64
# epochs数量，不用太大，因为句子数量较多
epochs = 200
# 多少步保存一次模型，防止程序崩溃导致模型丢失。
save_after_step = 100

# 是否使用缓存，由于文件较大，初始化动作较慢，所以将初始化好的文件持久化
use_cache = True

# 定义训练设备
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

print("batch_size:", batch_size)
print("每{}步保存一次模型".format(save_after_step))
print("Device:", device)